#################################################################################################
#################################################################################################
## This script computes the critical values of the asymptotic distribution of T using the
## representation of the limit Gaussian process via spherical harmonics from the main manuscript.
## It is only computed for dimension d=2, because at the time of coding this script there was
## no stable implementation of orthonormal spherical harmonics in higher dimensions and orders.
#################################################################################################
#################################################################################################

# required packages
library(Directional)
library(MASS)
library(MonteCarlo)
library(data.table)

setwd("/home/jaroslav-borodavka/Schreibtisch/Dissertation/Arbeit/Bernoulli Submission/codes/codes_git_project/R")

#################################################################################################
## general functions ## 

# uniformly distributed vectors on S^(d-1)
runif_sphere <- function(n, d){
  X = mvrnorm(n, rep(0,d), diag(1,d))
  if (n == 1){
    temp = as.vector(crossprod(X))
    U = X/sqrt(temp)
  }
  else{
    temp = diag(X %*% t(X))
    U = matrix(0, nrow = n, ncol = d)
    for (j in 1:n){ 
      U[j,] = X[j,]/sqrt(temp[j])             
    }
  }
  return(U)
}

# surface area of the unit hypersphere
surface_area <- function(d){
  return(2*pi^(d/2)/gamma(d/2))
}

# eigenvalues as in the end of section 2 of the main manuscript, beta <= 6
eigenvalue <- function(beta, d){
  eigenvalue_list = switch (beta,
                      c(0, 1/d^2 ),
                      c(0, 0, (2/(d*(d+2)))^2 ),
                      c(0, (3/(d*(d+2)))^2, 0, (6/(d*(d+2)*(d+4)))^2 ),
                      c(0, 0, (12/(d*(d+2)*(d+4)))^2, 0, (24/(d*(d+2)*(d+4)*(d+6)))^2 ),
                      c(0, (15/(d*(d+2)*(d+4)))^2, 0, (60/(d*(d+2)*(d+4)*(d+6)))^2, 0, (120/(d*(d+2)*(d+4)*(d+6)*(d+8)))^2 ),
                      c(0, 0, (90/(d*(d+2)*(d+4)*(d+6)))^2, 0, (360/(d*(d+2)*(d+4)*(d+6)*(d+8)))^2, 0, (720/(d*(d+2)*(d+4)*(d+6)*(d+8)*(d+10)))^2 )
  )
  return(eigenvalue_list)
}

# dimension of the space of d-dimensional spherical harmonics of order k
dim_Har <- function(k, d){
  return(if (k==0 & d==2) 1 else (2*k+d-2)/(k+d-2)*choose(k+d-2, d-2))
}

#################################################################################################
## functions, only d=2 ## 

# orthonormal basis computed via HFT.m in Mathematica,
# order k in {0,...,beta}, beta <= 6, vector x in S^1 
SphericalHar_ONB <- function(k){
  func_list = switch (k+1,
                list(function(x) 1/sqrt(2*pi)),
                list(function(x) x[2]/sqrt(pi), 
                     function(x) x[1]/sqrt(pi)),
                list(function(x) (1-2*x[2]^2)/sqrt(pi), 
                     function(x) (2*x[1]*x[2])/sqrt(pi)),
                list(function(x) (3*x[2]-4*x[2]^3)/sqrt(pi), 
                     function(x) (x[1]-4*x[1]*x[2]^2)/sqrt(pi)),
                list(function(x) (1-8*x[2]^2+8*x[2]^4)/sqrt(pi), 
                     function(x) 4*(x[1]*x[2]-2*x[1]*x[2]^3)/sqrt(pi)),
                list(function(x) (5*x[2]-20*x[2]^3+16*x[2]^5)/sqrt(pi), 
                     function(x) (x[1]-12*x[1]*x[2]^2+16*x[1]*x[2]^4)/sqrt(pi)),
                list(function(x) (1-18*x[2]^2+48*x[2]^4-32*x[2]^6)/sqrt(pi), 
                     function(x) 2*(3*x[1]*x[2]-16*x[1]*x[2]^3+16*x[1]*x[2]^5)/sqrt(pi))
  )
  return(func_list)
}


# limit Gaussian process via representation formula of section 2 of the main manuscript
GP <- function(b, beta, normal_array){
  res = 0
  for (k in seq(0, beta)) {
    for (j in seq(1, dim_Har(k, 2))) {
      res = res + sqrt(eigenvalue(beta, 2)[k+1])*SphericalHar_ONB(k)[[j]](b)*normal_array[[k+1]][j]
    }
  }
  return(sqrt(surface_area(2))*res)
}

# approximated realization of the limit distribution of T
T_SpherGP_stat_value <- function(d, beta){
  m = 2500
  runif_cover = runif_sphere(m, d)
  rnorm_array = list()
  for (k in seq(0, beta)) {
    rnorm_array[[k+1]] = 0
    for (j in seq(1, dim_Har(k, 2))) {
      rnorm_array[[k+1]][j] = rnorm(1, mean = 0, sd = 1)
    }
  }
  Z_beta = apply(runif_cover, 1, GP, beta = beta, normal_array = rnorm_array)
  return(list("realization" = max(Z_beta^2)))
}

#################################################################################################
## calculation of critical values of the asymptotic distribution of T via Monte Carlo ## 

# replication number l (cf. main manuscript, section 6) for critical values
l = 20000                                                  
# hyperparameters
numCores = 18
dim_grid = c(2)
beta_grid = seq(1,6)
grp_number = length(dim_grid)*length(beta_grid)

# parameter constellation for MonteCarlo package
parameter = list("d" = dim_grid, "beta" = beta_grid)  

# export of required data, packages and functions
export_list = list("functions" = c("GP", "eigenvalue", "surface_area", "dim_Har", "SphericalHar_ONB"), 
                   "packages" = c("MASS", "Directional"))

# Monte Carlo loops
res_T_SpherGP <- MonteCarlo::MonteCarlo(T_SpherGP_stat_value, nrep = l, param_list = parameter, 
                                        ncpus = numCores, time_n_test = FALSE, export_also = export_list)      
# producing a data.frame with all realizations
DF_T_SpherGP <- MonteCarlo::MakeFrame(res_T_SpherGP)                   
summary(DF_T_SpherGP)

# converting to a data.table
DT_T_SpherGP <- setDT(DF_T_SpherGP)

# creating groups
DT_T_SpherGP[, grp := .GRP, by = c("d", "beta")]                  
DT_T_SpherGP = DT_T_SpherGP[order(grp, realization)]
DT_T_SpherGP = DT_T_SpherGP[, list("dimension" = d, "beta" = beta, "realization" = realization, 
                           "level_10" = lapply(.SD, quantile, probs = 0.90),
                           "level_5" = lapply(.SD, quantile, probs = 0.95),
                           "level_1" = lapply(.SD, quantile, probs = 0.99)), 
                    by = grp, .SDcols = c("realization")]
print(DT_T_SpherGP)

save.image(file = "T_Statistic_GP_viaSphericalHarmonics_data.Rdata")