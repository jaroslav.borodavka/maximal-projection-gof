(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     23283,        585]
NotebookOptionsPosition[     21119,        545]
NotebookOutlinePosition[     21511,        561]
CellTagsIndexPosition[     21468,        558]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"<<", 
  "\"\</Users/faik/Desktop/Mathematik/Masterarbeit/2. Ansatz/Mathematica \
Packages/HFT12.m\>\""}]], "Input",
 CellChangeTimes->{{3.870486172442062*^9, 3.87048617891928*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"e1cfed86-395b-4280-aa2f-880a1917cafb"],

Cell[CellGroupData[{

Cell[BoxData["\<\"HFT12.m, version 12.03, 20 December 2020; for use with \
\\!\\(\\*\\nStyleBox[\\\"Mathematica\\\",\\nFontSlant->\\\"Italic\\\"]\\) \
versions 7 through 12 (and probably later versions of \\!\\(\\*\\nStyleBox[\\\
\"Mathematica\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\")\\\
\",\\nFontSlant->\\\"Italic\\\"]\\).\"\>"], "Print",
 CellChangeTimes->{3.870486180095374*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"2a8e4c58-e3c8-4c63-a850-202b91fd69bf"],

Cell[BoxData["\<\"The HFT12.m \
\\!\\(\\*\\nStyleBox[\\\"Mathematica\\\",\\nFontSlant->\\\"Italic\\\"]\\) \
package is designed for computing with harmonic functions.\"\>"], "Print",
 CellChangeTimes->{3.870486180101404*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"7704f751-0ec2-4d78-bef4-62a0de60678e"],

Cell[BoxData["\<\"Documentation for the use of this package and information \
about the algorithms used in it is available in the document titled \\!\\(\\*\
\\nStyleBox[\\\"Computing\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\\
nStyleBox[\\\" \
\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\"with\\\",\\\
nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\" \\\",\\nFontSlant->\\\
\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\"Harmonic\\\",\\nFontSlant->\\\"\
Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\" \\\",\\nFontSlant->\\\"Italic\\\"]\\)\
\\!\\(\\*\\nStyleBox[\\\"Functions\\\",\\nFontSlant->\\\"Italic\\\"]\\), \
which is available in both nb and pdf formats.\"\>"], "Print",
 CellChangeTimes->{3.870486180107625*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"c69a48c6-a3fb-4b14-9655-6775c9e5a584"],

Cell[BoxData["\<\"The most recent version of this HFT12.m package and its \
documentation \\!\\(\\*\\nStyleBox[\\\"Computing\\\",\\nFontSlant->\\\"Italic\
\\\"]\\)\\!\\(\\*\\nStyleBox[\\\" \\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\
\\*\\nStyleBox[\\\"with\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\\
nStyleBox[\\\" \
\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\"Harmonic\\\",\\\
nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\" \\\",\\nFontSlant->\\\
\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\"Functions\\\",\\nFontSlant->\\\"\
Italic\\\"]\\) are available at http://www.axler.net.\"\>"], "Print",
 CellChangeTimes->{3.8704861801136303`*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"56bfe015-3780-4794-a251-ddf2d984c1b9"],

Cell[BoxData["\<\"For addional information about harmonic functions, see the \
book \\!\\(\\*\\nStyleBox[\\\"Harmonic\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\
\\(\\*\\nStyleBox[\\\" \
\\\",\\nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\"Function\\\",\\\
nFontSlant->\\\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\" \\\",\\nFontSlant->\\\
\"Italic\\\"]\\)\\!\\(\\*\\nStyleBox[\\\"Theory\\\",\\nFontSlant->\\\"Italic\\\
\"]\\) (second edition), by Sheldon Axler, Paul Bourdon, and Wade Ramey, \
published by Springer.\"\>"], "Print",
 CellChangeTimes->{3.8704861801189013`*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"caac04dc-60bf-4960-b614-3495bfb66a66"],

Cell[BoxData["\<\"This package is copyrighted by Sheldon Axler but is \
distributed without charge.\"\>"], "Print",
 CellChangeTimes->{3.870486180125358*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"93520997-3685-4007-82cb-5558a848b57e"],

Cell[BoxData["\<\"Comments, suggestions, and bug reports should be sent to \
axler@sfsu.edu.\"\>"], "Print",
 CellChangeTimes->{3.8704861801310472`*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"3238173b-9c31-4c6b-8e76-b3769f28f406"]
}, Open  ]],

Cell[BoxData[
 TemplateBox[{
  "SetDelayed", "nosym", 
   "\"\\!\\(\\*RowBox[{\\\"f_\\\"}]\\) does not contain a symbol to attach a \
rule to.\"", 2, 3, 2, 22985123818399949143, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{3.8704861801390867`*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"da30a0c4-a7f9-4f18-b0d1-7a8879b3fc52"],

Cell[BoxData["\<\"* You can now use the functions in this package.\"\>"], \
"Print",
 CellChangeTimes->{3.870486180156287*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"29664084-9aa7-4286-acd5-d9ee8a0c7e01"]
}, Open  ]],

Cell[BoxData[{"Funktionen", "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"innerProduct", "[", 
   RowBox[{"f_", ",", " ", "g_", ",", " ", "x_"}], "]"}], " ", ":=", " ", 
  RowBox[{
   RowBox[{"surfaceArea", "[", 
    RowBox[{"dimension", "[", "x", "]"}], "]"}], "*", 
   RowBox[{"integrateSphere", "[", 
    RowBox[{
     RowBox[{"f", " ", "g"}], ",", " ", "x"}], 
    "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sphericalONB", "[", 
   RowBox[{"k_", ",", " ", "x_"}], "]"}], " ", ":=", " ", 
  RowBox[{
   RowBox[{"basisH", "[", 
    RowBox[{"k", ",", " ", "x", ",", " ", "innerProduct"}], "]"}], " ", "/.", 
   " ", 
   RowBox[{
    RowBox[{"norm", "[", "x", "]"}], "->", " ", "1"}]}]}]}], "Input",
 CellChangeTimes->{{3.8704896933668947`*^9, 3.870489745741026*^9}, {
  3.870489776339625*^9, 3.8704898252307177`*^9}, {3.8704901457439747`*^9, 
  3.870490146650282*^9}, {3.87049030716901*^9, 3.870490310388753*^9}, {
  3.870490389184462*^9, 3.870490392998625*^9}, {3.870490505735209*^9, 
  3.8704905206561193`*^9}, {3.870490558804525*^9, 3.870490688426074*^9}, {
  3.870490840850297*^9, 3.870490896910693*^9}, {3.870491210952025*^9, 
  3.870491218302731*^9}, {3.8704915898057623`*^9, 
  3.870491596629051*^9}},ExpressionUUID->"cf465b42-1905-4467-916f-\
cf8e9526b7b4"],

Cell[CellGroupData[{

Cell[BoxData[{"Variablen", "\[IndentingNewLine]", 
 RowBox[{"dim", " ", "=", " ", "3"}], "\[IndentingNewLine]", 
 RowBox[{"setDimension", "[", 
  RowBox[{"x", ",", " ", "dim"}], "]"}]}], "Input",
 CellChangeTimes->{{3.870489832543214*^9, 3.8704898349877453`*^9}, {
  3.870490010461664*^9, 3.870490017635078*^9}, {3.870490149969112*^9, 
  3.870490166747735*^9}, {3.870490322664249*^9, 3.87049033835709*^9}, {
  3.8704907725575027`*^9, 3.870490796916988*^9}, {3.870491316803426*^9, 
  3.8704913407946463`*^9}, {3.870491503628488*^9, 3.870491504202609*^9}, {
  3.870491570127345*^9, 3.870491602341568*^9}, {3.870493767941457*^9, 
  3.8704937684237947`*^9}, {3.870494367553597*^9, 3.870494368243668*^9}, {
  3.8704967431352577`*^9, 3.870496743321417*^9}, {3.87049678658355*^9, 
  3.870496786716641*^9}},
 CellLabel->
  "In[124]:=",ExpressionUUID->"5682e65f-272f-4b73-9fe8-691243395571"],

Cell[BoxData["Variablen"], "Output",
 CellChangeTimes->{
  3.870491203336302*^9, {3.870491318214382*^9, 3.870491341383782*^9}, 
   3.870491504818737*^9, 3.870491573627438*^9, 3.870493769769833*^9, 
   3.870494369001395*^9, 3.870496744769639*^9, 3.870496787681926*^9},
 CellLabel->
  "Out[124]=",ExpressionUUID->"9c9bafa5-09c1-4691-9c15-f72dfc510c40"],

Cell[BoxData["3"], "Output",
 CellChangeTimes->{
  3.870491203336302*^9, {3.870491318214382*^9, 3.870491341383782*^9}, 
   3.870491504818737*^9, 3.870491573627438*^9, 3.870493769769833*^9, 
   3.870494369001395*^9, 3.870496744769639*^9, 3.8704967876881847`*^9},
 CellLabel->
  "Out[125]=",ExpressionUUID->"e71e071a-a908-4c24-8473-82bef28dde8e"],

Cell[BoxData[
 TemplateBox[{
  "setDimension", "Vector", 
   "\"\\!\\(\\*RowBox[{\\\"x\\\"}]\\) will be considered to be a vector in \
\\!\\(\\*RowBox[{\\\"3\\\"}]\\)-dimensional real Euclidean space.\"", 2, 126, 
   17, 22985123818399949143, "Local", "HFT`setDimension"},
  "MessageTemplate2"]], "Message", "MSG",
 CellChangeTimes->{
  3.8704912034519463`*^9, {3.8704913183261414`*^9, 3.870491341490587*^9}, 
   3.870491504922008*^9, 3.870491573728325*^9, 3.870493769905712*^9, 
   3.870494369113317*^9, 3.870496744885016*^9, 3.870496787790663*^9},
 CellLabel->
  "During evaluation of \
In[124]:=",ExpressionUUID->"e27d0cc8-fa54-4a43-b2d5-1f32e3192fc6"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.870494372399724*^9, 
  3.870494373018301*^9}},ExpressionUUID->"ba00170f-75d7-4566-8bb0-\
7a0f7023973c"],

Cell[CellGroupData[{

Cell[BoxData[{"Berechnung", "\[IndentingNewLine]", 
 RowBox[{"sphericalONB", "[", 
  RowBox[{"6", ",", " ", "x"}], "]"}]}], "Input",
 CellChangeTimes->{{3.8704907334327602`*^9, 3.870490755460247*^9}, {
  3.8704909109601088`*^9, 3.87049091333438*^9}, {3.8704913470621147`*^9, 
  3.870491350311275*^9}, {3.8704915478116913`*^9, 3.870491558466278*^9}, {
  3.870491605257839*^9, 3.870491621116515*^9}, {3.870493554919497*^9, 
  3.870493674761097*^9}, {3.8704937354198837`*^9, 3.87049374150238*^9}, {
  3.870493816853335*^9, 3.8704938177201853`*^9}, {3.870493917235219*^9, 
  3.870493930403517*^9}, {3.870493970300853*^9, 3.870493973358171*^9}, {
  3.870494093191657*^9, 3.870494093324707*^9}, {3.870494382500165*^9, 
  3.870494382631176*^9}, {3.870494488191614*^9, 3.870494488346542*^9}, {
  3.870496534629538*^9, 3.8704965347498627`*^9}, {3.870496571457509*^9, 
  3.870496571721949*^9}, {3.8704967550418367`*^9, 3.8704967551733713`*^9}, {
  3.870496789579309*^9, 
  3.870496820766029*^9}},ExpressionUUID->"ea7f7f6b-bc65-4129-8ac1-\
a41ee62c233d"],

Cell[BoxData["Berechnung"], "Output",
 CellChangeTimes->{
  3.870491350759914*^9, 3.8704915096832647`*^9, {3.870491559311357*^9, 
   3.87049157685814*^9}, 3.8704916229492493`*^9, 3.870493578423592*^9, {
   3.8704936146635*^9, 3.870493619053958*^9}, {3.8704936550713053`*^9, 
   3.870493675465417*^9}, {3.870493736484335*^9, 3.870493742058133*^9}, 
   3.870493773085886*^9, {3.8704939187440233`*^9, 3.8704939358909073`*^9}, 
   3.8704939742510977`*^9, 3.870494013703713*^9, 3.870494319300103*^9, {
   3.8704943795489483`*^9, 3.8704943830408287`*^9}, 3.870494489062595*^9, 
   3.8704965377485323`*^9, 3.870496572233143*^9, {3.870496747259975*^9, 
   3.87049675569888*^9}, {3.87049679035791*^9, 3.870496814746934*^9}},
 CellLabel->
  "Out[133]=",ExpressionUUID->"2eccd67f-8366-43fb-8d83-a912861100a1"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    FractionBox["1", "32"], " ", 
    SqrtBox[
     FractionBox["13", "\[Pi]"]], " ", 
    RowBox[{"(", 
     RowBox[{"5", "-", 
      RowBox[{"105", " ", 
       SubsuperscriptBox["x", "2", "2"]}], "+", 
      RowBox[{"315", " ", 
       SubsuperscriptBox["x", "2", "4"]}], "-", 
      RowBox[{"231", " ", 
       SubsuperscriptBox["x", "2", "6"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "16"], " ", 
    SqrtBox[
     FractionBox["273", "\[Pi]"]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"5", " ", 
       SubscriptBox["x", "2"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"30", " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubscriptBox["x", "3"]}], "+", 
      RowBox[{"33", " ", 
       SubsuperscriptBox["x", "2", "5"], " ", 
       SubscriptBox["x", "3"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "32"], " ", 
    SqrtBox[
     FractionBox["1365", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", 
      RowBox[{"19", " ", 
       SubsuperscriptBox["x", "2", "2"]}], "+", 
      RowBox[{"51", " ", 
       SubsuperscriptBox["x", "2", "4"]}], "-", 
      RowBox[{"33", " ", 
       SubsuperscriptBox["x", "2", "6"]}], "-", 
      RowBox[{"2", " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"36", " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "-", 
      RowBox[{"66", " ", 
       SubsuperscriptBox["x", "2", "4"], " ", 
       SubsuperscriptBox["x", "3", "2"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "16"], " ", 
    SqrtBox[
     FractionBox["1365", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"9", " ", 
       SubscriptBox["x", "2"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"42", " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubscriptBox["x", "3"]}], "+", 
      RowBox[{"33", " ", 
       SubsuperscriptBox["x", "2", "5"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"12", " ", 
       SubscriptBox["x", "2"], " ", 
       SubsuperscriptBox["x", "3", "3"]}], "+", 
      RowBox[{"44", " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubsuperscriptBox["x", "3", "3"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["3", "32"], " ", 
    SqrtBox[
     FractionBox["91", "\[Pi]"]], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", 
      RowBox[{"13", " ", 
       SubsuperscriptBox["x", "2", "2"]}], "+", 
      RowBox[{"23", " ", 
       SubsuperscriptBox["x", "2", "4"]}], "-", 
      RowBox[{"11", " ", 
       SubsuperscriptBox["x", "2", "6"]}], "-", 
      RowBox[{"8", " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"96", " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "-", 
      RowBox[{"88", " ", 
       SubsuperscriptBox["x", "2", "4"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"8", " ", 
       SubsuperscriptBox["x", "3", "4"]}], "-", 
      RowBox[{"88", " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "4"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["3", "16"], " ", 
    SqrtBox[
     FractionBox["1001", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"5", " ", 
       SubscriptBox["x", "2"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"10", " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubscriptBox["x", "3"]}], "+", 
      RowBox[{"5", " ", 
       SubsuperscriptBox["x", "2", "5"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"20", " ", 
       SubscriptBox["x", "2"], " ", 
       SubsuperscriptBox["x", "3", "3"]}], "+", 
      RowBox[{"20", " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubsuperscriptBox["x", "3", "3"]}], "+", 
      RowBox[{"16", " ", 
       SubscriptBox["x", "2"], " ", 
       SubsuperscriptBox["x", "3", "5"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "32"], " ", 
    SqrtBox[
     FractionBox["3003", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", 
      RowBox[{"3", " ", 
       SubsuperscriptBox["x", "2", "2"]}], "+", 
      RowBox[{"3", " ", 
       SubsuperscriptBox["x", "2", "4"]}], "-", 
      SubsuperscriptBox["x", "2", "6"], "-", 
      RowBox[{"18", " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"36", " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "-", 
      RowBox[{"18", " ", 
       SubsuperscriptBox["x", "2", "4"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"48", " ", 
       SubsuperscriptBox["x", "3", "4"]}], "-", 
      RowBox[{"48", " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "4"]}], "-", 
      RowBox[{"32", " ", 
       SubsuperscriptBox["x", "3", "6"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "16"], " ", 
    SqrtBox[
     FractionBox["273", "\[Pi]"]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"5", " ", 
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "2"]}], "-", 
      RowBox[{"30", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "3"]}], "+", 
      RowBox[{"33", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "5"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "16"], " ", 
    SqrtBox[
     FractionBox["1365", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"18", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubscriptBox["x", "3"]}], "+", 
      RowBox[{"33", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "4"], " ", 
       SubscriptBox["x", "3"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "16"], " ", 
    SqrtBox[
     FractionBox["1365", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"3", " ", 
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "2"]}], "-", 
      RowBox[{"14", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "3"]}], "+", 
      RowBox[{"11", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "5"]}], "-", 
      RowBox[{"12", " ", 
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "2"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"44", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubsuperscriptBox["x", "3", "2"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["3", "8"], " ", 
    SqrtBox[
     FractionBox["91", "\[Pi]"]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"12", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubscriptBox["x", "3"]}], "+", 
      RowBox[{"11", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "4"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"2", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "3", "3"]}], "+", 
      RowBox[{"22", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "3"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["3", "16"], " ", 
    SqrtBox[
     FractionBox["1001", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "2"]}], "-", 
      RowBox[{"2", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "3"]}], "+", 
      RowBox[{
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "5"]}], "-", 
      RowBox[{"12", " ", 
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "2"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"12", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "3"], " ", 
       SubsuperscriptBox["x", "3", "2"]}], "+", 
      RowBox[{"16", " ", 
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "2"], " ", 
       SubsuperscriptBox["x", "3", "4"]}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "16"], " ", 
    SqrtBox[
     FractionBox["3003", 
      RowBox[{"2", " ", "\[Pi]"}]]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"3", " ", 
       SubscriptBox["x", "1"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"6", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubscriptBox["x", "3"]}], "+", 
      RowBox[{"3", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "4"], " ", 
       SubscriptBox["x", "3"]}], "-", 
      RowBox[{"16", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "3", "3"]}], "+", 
      RowBox[{"16", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "2", "2"], " ", 
       SubsuperscriptBox["x", "3", "3"]}], "+", 
      RowBox[{"16", " ", 
       SubscriptBox["x", "1"], " ", 
       SubsuperscriptBox["x", "3", "5"]}]}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.870491350759914*^9, 3.8704915096832647`*^9, {3.870491559311357*^9, 
   3.87049157685814*^9}, 3.8704916229492493`*^9, 3.870493578423592*^9, {
   3.8704936146635*^9, 3.870493619053958*^9}, {3.8704936550713053`*^9, 
   3.870493675465417*^9}, {3.870493736484335*^9, 3.870493742058133*^9}, 
   3.870493773085886*^9, {3.8704939187440233`*^9, 3.8704939358909073`*^9}, 
   3.8704939742510977`*^9, 3.870494013703713*^9, 3.870494319300103*^9, {
   3.8704943795489483`*^9, 3.8704943830408287`*^9}, 3.870494489062595*^9, 
   3.8704965377485323`*^9, 3.870496572233143*^9, {3.870496747259975*^9, 
   3.87049675569888*^9}, {3.87049679035791*^9, 3.870496815008697*^9}},
 CellLabel->
  "Out[134]=",ExpressionUUID->"d932fa64-0281-4bc2-ade9-92811b8853a1"]
}, Open  ]]
},
WindowSize->{1440, 847},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.1 for Mac OS X x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"c92bd190-0f2d-4b5e-9449-7358034bfb8f"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 282, 5, 30, "Input",ExpressionUUID->"e1cfed86-395b-4280-aa2f-880a1917cafb"],
Cell[CellGroupData[{
Cell[887, 31, 508, 8, 24, "Print",ExpressionUUID->"2a8e4c58-e3c8-4c63-a850-202b91fd69bf"],
Cell[1398, 41, 328, 6, 24, "Print",ExpressionUUID->"7704f751-0ec2-4d78-bef4-62a0de60678e"],
Cell[1729, 49, 839, 13, 44, "Print",ExpressionUUID->"c69a48c6-a3fb-4b14-9655-6775c9e5a584"],
Cell[2571, 64, 774, 12, 24, "Print",ExpressionUUID->"56bfe015-3780-4794-a251-ddf2d984c1b9"],
Cell[3348, 78, 683, 11, 24, "Print",ExpressionUUID->"caac04dc-60bf-4960-b614-3495bfb66a66"],
Cell[4034, 91, 261, 5, 24, "Print",ExpressionUUID->"93520997-3685-4007-82cb-5558a848b57e"],
Cell[4298, 98, 256, 5, 24, "Print",ExpressionUUID->"3238173b-9c31-4c6b-8e76-b3769f28f406"]
}, Open  ]],
Cell[4569, 106, 376, 9, 28, "Message",ExpressionUUID->"da30a0c4-a7f9-4f18-b0d1-7a8879b3fc52"],
Cell[4948, 117, 230, 5, 24, "Print",ExpressionUUID->"29664084-9aa7-4286-acd5-d9ee8a0c7e01"]
}, Open  ]],
Cell[5193, 125, 1278, 28, 73, "Input",ExpressionUUID->"cf465b42-1905-4467-916f-cf8e9526b7b4"],
Cell[CellGroupData[{
Cell[6496, 157, 882, 14, 73, "Input",ExpressionUUID->"5682e65f-272f-4b73-9fe8-691243395571"],
Cell[7381, 173, 350, 6, 34, "Output",ExpressionUUID->"9c9bafa5-09c1-4691-9c15-f72dfc510c40"],
Cell[7734, 181, 344, 6, 34, "Output",ExpressionUUID->"e71e071a-a908-4c24-8473-82bef28dde8e"],
Cell[8081, 189, 655, 13, 28, "Message",ExpressionUUID->"e27d0cc8-fa54-4a43-b2d5-1f32e3192fc6"]
}, Open  ]],
Cell[8751, 205, 152, 3, 30, "Input",ExpressionUUID->"ba00170f-75d7-4566-8bb0-7a0f7023973c"],
Cell[CellGroupData[{
Cell[8928, 212, 1043, 16, 52, "Input",ExpressionUUID->"ea7f7f6b-bc65-4129-8ac1-a41ee62c233d"],
Cell[9974, 230, 798, 12, 34, "Output",ExpressionUUID->"2eccd67f-8366-43fb-8d83-a912861100a1"],
Cell[10775, 244, 10328, 298, 254, "Output",ExpressionUUID->"d932fa64-0281-4bc2-ade9-92811b8853a1"]
}, Open  ]]
}
]
*)

